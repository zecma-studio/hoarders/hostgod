﻿using System;
using System.Security.Claims;
using Entities;
using Helpers.Response;
using HostGod.Entities;
using HostGod.Helpers.Request;
using HostGod.Models.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Helpers.Auth;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HostGod.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HostController : ControllerBase
    {

        private readonly IHostModel _hostModel;
        public HostController(IHostModel hostModel)
        {
            _hostModel = hostModel;
        }
        // GET: api/<Host>
        [HttpGet]
        [Authorize]
        public ActionResult<Response> Get()
        {
            try
            {
                var hosts = _hostModel.GetHostList();
                return new Response { data = hosts, code = 200 };

            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }

        // GET api/<Host>/5
        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<Response> Get(int id)
        {
           
            try
            {
                int userId =HeadersHelper.GetIdFromAuth(HttpContext);
                //User not found
                if (userId == 0) throw new Exception("Headers missmatch");
                Host host = _hostModel.GetById(id);
                if (host == null)
                {
                    return NotFound(new Response { code = 404 });

                }
                return new Response { data = host, code = 200 };

            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }

        // POST api/<Host>
        [HttpPost]
        [Authorize]
        public ActionResult<Response> Post([FromBody] CreateHostRequest request)
        {
            try
            {
                int userId = HeadersHelper.GetIdFromAuth(HttpContext);
                //User not found
                if (userId == 0) throw new Exception("Headers missmatch");
                Host host= _hostModel.SaveHost(request.Email, request.Username, request.Password, request.LegalFirstName, request.LegalLastName, request.LegalEmail, request.LegalAddress, request.LegalPhone, request.LegalCountry, request.OficialID, request.PublicEmail, request.Avatar, userId);
                if (host == null)
                {
                    return Conflict(new Response { message = "Host already registered", code = 401 });

                }
                return new Response { data = host, code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }

        // PUT api/<Host>/5
        [HttpPut("{id}")]
        [Authorize]
        public void Put(int id, [FromBody] UpdateHostRequest request)
        {
        }

        // DELETE api/<Host>/5
        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<Response> Delete(int id)
        {
            try
            {
                bool deleted = _hostModel.DeleteHost(id);
                if (!deleted)
                {
                    return NotFound(new Response { message = "NOT_FOUND", code = 404 });
                }
                return new Response { message = "OK", code = 200 };

            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });
            }
        }
    }
}

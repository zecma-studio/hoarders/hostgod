﻿using Entities.Zeclogin;
using HostGod.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HostGod.Models.Interfaces
{
    public interface IHostModel
    {

        IEnumerable<Host> GetHostList();
        Host GetById(int id);
        Host SaveHost(string email, string username, string password, string firstName, string lastName, string legalEmail, string legalAddress, string legalPhone, string legalCountry, string oficialID, string publicEmail, string avatar, int createdById);
        //Return true successful
        bool DeleteHost(int id);
    }
}

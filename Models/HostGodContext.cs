﻿using Microsoft.EntityFrameworkCore;
using HostGod.Entities;

namespace HostGod.Models
{
    public class HostGodContext: DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseNpgsql("Host=localhost;Database=hostgod;Username=postgres;Password=docker");

        public DbSet<Host> Hosts { get; set; }

    }
}

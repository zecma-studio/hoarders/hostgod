﻿using System;
using System.Collections.Generic;
using Entities.Zeclogin;
using HostGod.Entities;
using HostGod.Models.Interfaces;
using HostGod.Services;
using Helpers.Encrypt;

namespace HostGod.Models
{
    public class HostModel : IHostModel
    {

        private readonly IHostService _hostService;
        public HostModel(IHostService hostService) 
        {
            this._hostService = hostService;
        }
        public bool DeleteHost(int id)
        {
            bool success = _hostService.DeleteHost(id);
            return success;
            
        }

        public Host GetById(int id)
        {
            Host host = _hostService.GetById(id);
            return host;
        }

        public IEnumerable<Host> GetHostList()
        {
            IEnumerable<Host> hosts = _hostService.GetHostList();
            return hosts;
        }

        public Host SaveHost(string email, string username, string password, string firstName, string lastName, string legalEmail, string legalAddres, string legalPhone, string legalCountry, string oficialID, string publicEmail, string avatar, int createdBy)
        {
            Dictionary<string, string> hashedPass= PasswordHelper.EncryptPassword(password);
            Host host = _hostService.SaveHost(email, username, hashedPass["salt"], hashedPass["hash"], firstName, lastName, legalEmail, legalAddres, legalPhone, legalCountry, oficialID, publicEmail, avatar, createdBy);
            return host;
        }
    }
}

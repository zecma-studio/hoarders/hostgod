﻿using Entities.Zeclogin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HostGod.Helpers.Request
{
    public class CreateHostRequest
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string LegalFirstName { get; set; }
        [Required]
        public string LegalLastName { get; set; }
        [Required]
        public string LegalEmail { get; set; }
        [Required]
        public string LegalPhone { get; set; }
        [Required]
        public string LegalAddress { get; set; }
        [Required]
        public string LegalCountry { get; set; }
        [Required]
        public string OficialID { get; set; }
        [Required]
        public string PublicEmail { get; set; }
        public bool Verified { get; set; }
        public float Reputation { get; set; }
        public string Avatar { get; set; }
    }
}

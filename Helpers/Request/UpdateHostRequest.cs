﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HostGod.Helpers.Request
{
    public class UpdateHostRequest
    {

        public string Username { get; set; }
        public string Password { get; set; }
        public string LegalFirstName { get; set; }
        public string LegalLastName { get; set; }
        public string LegalEmail { get; set; }
        public string LegalPhone { get; set; }
        public string LegalAddress { get; set; }
        public string LegalCountry { get; set; }
        public string OficialID { get; set; }
        public string PublicEmail { get; set; }
        public bool Verified { get; set; }
        public string Avatar { get; set; }
    }
}

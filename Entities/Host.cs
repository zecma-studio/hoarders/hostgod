﻿using Entities.Zeclogin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HostGod.Entities
{
    public class Host
    {

            public int Id { get; set; }
            public string Email { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public string Salt { get; set; }
            [Required]
            public string LegalFirstName { get; set; }
            [Required]
            public string LegalLastName { get; set; }
            [Required]
            public string LegalEmail { get; set; }
            [Required]
            public string LegalPhone { get; set; }
            [Required]
            public string LegalAddress { get; set; }
            [Required]
            public string LegalCountry { get; set; }
            [Required]
            public string OficialID { get; set; }
            public string PublicEmail { get; set; }
            public bool Verified { get; set; }
            public float Reputation { get; set; }
            public string Avatar { get; set; }
            [Required]
            public int CreatedById { get; set; }

            public Host()
            {
                this.Verified = false;
                this.Reputation = 0f;
            }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace HostGod.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Hosts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Email = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Salt = table.Column<string>(nullable: true),
                    LegalFirstName = table.Column<string>(nullable: false),
                    LegalLastName = table.Column<string>(nullable: false),
                    LegalEmail = table.Column<string>(nullable: false),
                    LegalPhone = table.Column<string>(nullable: false),
                    LegalAddress = table.Column<string>(nullable: false),
                    LegalCountry = table.Column<string>(nullable: false),
                    OficialID = table.Column<string>(nullable: false),
                    PublicEmail = table.Column<string>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    Reputation = table.Column<float>(nullable: false),
                    Avatar = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hosts", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Hosts");
        }
    }
}

﻿using Entities.Zeclogin;
using HostGod.Entities;
using System.Collections.Generic;

namespace HostGod.Services
{
    public interface IHostService
    {
        IEnumerable<Host> GetHostList();
        Host GetById(int id);
        Host SaveHost(string email, string username, string salt, string password, string firstName, string lastName, string legalEmail, string legalAddress, string legalPhone, string legalCountry, string oficialID, string publicEmail, string avatar, int createdBy);
        //Return true successful
        bool DeleteHost(int id);
    }
}

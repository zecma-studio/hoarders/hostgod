﻿
using Entities.Zeclogin;
using HostGod.Entities;
using HostGod.Models;
using System.Collections.Generic;
using System.Linq;

namespace HostGod.Services.Postgres
{

    public class HostDBService : IHostService
    {
        private readonly HostGodContext context;

        public HostDBService(HostGodContext context)
        {
            this.context = context;
        }

        public bool DeleteHost(int id)
        {
            Host host = context.Hosts.FirstOrDefault(host => host.Id == id);
            if (host!= null)
            {
                context.Hosts.Remove(host);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public Host GetById(int id)
        {
            Host host = context.Hosts.FirstOrDefault(host => host.Id == id);
            return host;
        }

        public IEnumerable<Host> GetHostList()
        {
            IEnumerable<Host> hosts = context.Hosts.ToList();
            return hosts;
        }

        public Host SaveHost(string email, string username, string salt, string password, string firstName, string lastName, string legalEmail,string legalAddress, string legalPhone, string legalCountry, string oficialID, string publicEmail, string avatar, int createdBy)
        {
            Host host = context.Hosts.FirstOrDefault(host => host.Email == email || host.Username== username);
            if (host == null)
            {
                host = new Host { Email = email, Username = username, Salt = salt, Password = password, LegalFirstName = firstName, LegalLastName = lastName, LegalEmail = legalEmail, LegalAddress = legalAddress, LegalPhone = legalPhone, LegalCountry = legalCountry, OficialID = oficialID, PublicEmail = publicEmail, Avatar = avatar, CreatedById = createdBy};
                context.Hosts.Add(host);
                context.SaveChanges();
                return host;
            }
            return null;
        }
    }

}
